package main

import (
	"fmt"
	"reflect"
	"unicode/utf8"
)

func main() {

	//fmt.Printf("%T", "Hello World!")
	s := "G☺"
	fmt.Println("len: ", len(s))
	fmt.Println("len: ", utf8.RuneCountInString(s))

	// simple for loop gives access to bytes
	for i := 0; i < len(s); i++ {
		fmt.Printf("Value: %c, Unicode: %U, Type: %s\n", s[i], s[i], reflect.TypeOf(s[i]))
	}

	fmt.Println()

	// for range decodes a rune
	for _, value := range s {
		fmt.Printf("Value: %c, Unicode: %U, Type: %s\n", value, value, reflect.TypeOf(value))
	}

	//code point == rune == unicode character
	//strings in golang duality: sequence of bytes or collection of characters (runes)
	// byte (uint8)
	// rune (int32)

	banner("go", 6)
	banner("G☺", 6)
	x, y := 1, "1"
	fmt.Printf("x=%v, y=%v\n", x, y)   //Use #v in Debug
	fmt.Printf("x=%#v, y=%#v\n", x, y) //Use #v in Debug
	fmt.Printf("%20s!\n", s)

	fmt.Println(isPalindrome("G☺"))
	fmt.Println(isPalindrome("gog"))
	fmt.Println(isPalindrome("gogo"))
}

func isPalindrome(s string) bool {

	rs := []rune(s) //convert string into slice of runes
	for i := 0; i < len(rs)/2; i++ {
		if rs[i] != rs[len(rs)-i-1] {
			return false
		}
	}
	fmt.Println()

	return true
}

func banner(text string, width int) {
	//offset := (width - len(text)) / 2
	offset := (width - utf8.RuneCountInString(text)) / 2 //A rune is a code point character. It's an alias of int32.
	for i := 0; i < offset; i++ {
		fmt.Print(" ")
	}
	fmt.Println(text)
	for i := 0; i < width; i++ {
		fmt.Print("-")
	}
	fmt.Println()
}
